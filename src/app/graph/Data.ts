export class Data {
  constructor(public date: string,
              public open: number,
              public high: number,
              public low: number,
              public close: number,
              public volume: number = 10) {}
}
