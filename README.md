# Candlestick Generator
https://candlestick-generator-89bd7.web.app/ 

This project demonstrates the TechanJS Candlestick graphing library.  
There is a control panel provided underneath the graph that allows the user to play with different settings.  
For example:
  - Speed - how fast the graph draws the candlesticks. 
  - Candlesticks - how many candles appear within the graph.
## Tools
This project was made using Angular 7 and uses PrimeNG components.  
The graphing library is TechanJS.   
CI/CD & version control provided by Gitlab.  
The web app is hosted by Firebase.  

## Links
TechanJS: http://techanjs.org/  
PrimeFaces: https://www.primefaces.org/primeng/  


## Contributors
This project was made by Evan McLaughlin.   
https://gitlab.com/emclaug2/candlestick-stream?nav_source=navbar
