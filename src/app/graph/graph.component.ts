import {AfterViewInit, ChangeDetectorRef, Component} from '@angular/core';
import {drawDataPoint, initializeGraph, clearGraph, setMaxCandles} from './graph';
import {Data} from './Data';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements AfterViewInit {

  constructor(private ref: ChangeDetectorRef) {}

  interval: any;
  prevData: Data;

  neutralCheckbox = 'neutral';
  bullCheckbox = 'bull';
  bearCheckbox = 'bear';
  checkedValues: string[] = [this.neutralCheckbox];

  isPaused = true;
  isLoaded: boolean;
  isBull: boolean;
  isBear: boolean;
  isCrashLoaded: boolean;
  isPumpLoaded: boolean;

  day: number;
  month: number;
  year: number;
  speed = 50;
  maxSpeed = 100;
  maxCandles = 200;
  dipModifier = 40;
  pumpModifier = 40;

  ngAfterViewInit() {
    initializeGraph(this.maxCandles);
    this.redrawOnResize();
    this.resetMarket();
  }

  private redrawOnResize(): void {
    window.addEventListener('resize', (() => {
      initializeGraph(this.maxCandles);
    }));
  }

  resetMarket(): void {
    clearGraph();
    this.prevData = new Data(new Date().toDateString(), 1000, 1000, 1000, 1000, 1000);
    this.isCrashLoaded = false;
    this.isPumpLoaded = false;
    this.day = 16;
    this.month = 1;
    this.year = 19;
  }

  /* This is called repeatedly to create the data used in the graph library. */
  createInterval(): void {
    this.isLoaded = true;
    this.interval = setInterval((() => {
      if (this.day++ === 30) {
        this.day = 1;
        this.month += 1;
      } if (this.month === 12) {
        this.month = 1;
        this.year += 1;
      }
      const newData = this.generateData(this.prevData);
      drawDataPoint(newData);
      this.prevData = newData;
    }), this.maxSpeed - this.speed);
  }

  private generateData(prevData: Data): Data {
    const open = prevData.close;
    const close = this.calcClose(open);
    const high = this.calcHigh(open);
    const low = this.calcLow(open);
    const date = this.day + '-' + this.month + '-' + this.year;
    return new Data(date, open, high, low, close);
  }

  togglePause(): void {
    if (this.isPaused) {
      this.createInterval();
    } else {
      clearInterval(this.interval);
    }
    this.isPaused = !this.isPaused;
  }

  adjustSpeed(): void {
    if (!this.isPaused) {
      clearInterval(this.interval);
      this.createInterval();
    }
  }

  adjustMaxCandles(): void {
    setMaxCandles(this.maxCandles);
  }

  flashPump(): void {
    this.isPumpLoaded = true;
  }

  flashCrash(): void {
    this.isCrashLoaded = true;
  }

  /* Also modifies numbers based off of bear/bull market. */
  private calcClose(prevOpen: number): number {
    if (this.isPumpLoaded) {
      prevOpen += this.pumpModifier;
    }
    if (this.isCrashLoaded) {
      prevOpen -= this.dipModifier;
    }

    /* Determines which direction we are closing in. */
    const difference = Math.random() * 5; // 5 can be replaced with a slider named "market volatility"
    const close = prevOpen + (Boolean(Math.random() > .5) ? (difference) : (-difference));
    if (this.isBear) {
      return Math.floor(close);
    }
    if (this.isBull) {
      return Math.ceil(close);
    }
    return close;
  }

  private calcHigh(prevOpen: number): number {
    if (this.isPumpLoaded) {
      prevOpen += this.pumpModifier;
      this.isPumpLoaded = false;
    }
    return prevOpen + (Math.random() * 2);
  }

  private calcLow(prevOpen: number): number {
    if (this.isCrashLoaded) {
      prevOpen -= this.dipModifier;
      this.isCrashLoaded = false;
    }
    return prevOpen - (Math.random() * 2);
  }

  toggleNeutral(): void {
    this.checkedValues = [this.neutralCheckbox];
    this.isBull = false;
    this.isBear = false;
    this.ref.detectChanges();
  }

  toggleBear(): void {
    this.checkedValues = [this.bearCheckbox];
    this.isBull = false;
    this.isBear = true;
    this.ref.detectChanges();
  }

  toggleBull(): void {
    this.checkedValues = [this.bullCheckbox];
    this.isBear = false;
    this.isBull = true;
    this.ref.detectChanges();
  }
}
