import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PrimeModule} from './prime/prime.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  exports: [
    PrimeModule,
    FormsModule,
    BrowserAnimationsModule,
  ]
})
export class SharedModule { }
