/* Uses the techan library to manage candlestick creation. */
var techan = require('../../assets/js/techan.min');
var x, y, candlestick, xAxis, yAxis, svg, height, width;
var dataPoints = [];
var maxCandles;

export function clearGraph() {
  dataPoints = [];
  draw(dataPoints);
}

export function initializeGraph(maxHistoricCandles) {
  height = document.getElementById('graph-container').offsetHeight - 50;
  width = document.getElementById('graph-container').offsetWidth;

  // Remove old graph if it exists.
  d3.select("#graph-container").select("svg").remove();
  x = techan.scale.financetime().range([0, width]);
  y = d3.scaleLinear().range([height, 0]);
  candlestick = techan.plot.candlestick().xScale(x).yScale(y);
  xAxis = d3.axisBottom(x).ticks(4);
  yAxis = d3.axisLeft(y).ticks(8);
  setMaxCandles(maxHistoricCandles);

  svg = d3.select("#graph-container").append("svg")
    .append("g")
    .attr("transform", "translate(" + 40 + "," + 30 + ")");

    svg.append("g")
      .attr("class", "candlestick");

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")");

    svg.append("g")
      .attr("class", "y axis")
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Price")
      .attr("id", "y-desc");

  draw(dataPoints);
}

export function setMaxCandles(num) {
  maxCandles = num;
}

export function drawDataPoint(data) {
  const parseDate = d3.timeParse("%d-%m-%y");
  data.date = parseDate(data.date);
  dataPoints.push(data);
  if (dataPoints.length > maxCandles) {
    dataPoints = dataPoints.slice(dataPoints.length-maxCandles, dataPoints.length);
  }
  draw(dataPoints);
}

function draw(data) {
  x.domain(data.map(candlestick.accessor().d));
  y.domain(techan.scale.plot.ohlc(data, candlestick.accessor()).domain());

  svg.selectAll("g.candlestick").datum(data).call(candlestick);
  svg.selectAll("g.x.axis").call(xAxis);
  svg.selectAll("g.y.axis").call(yAxis);
}
