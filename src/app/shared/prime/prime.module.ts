import {ButtonModule} from 'primeng/button';
import {PanelModule} from 'primeng/panel';
import {NgModule} from '@angular/core';
import {SliderModule} from 'primeng/slider';
import {CheckboxModule} from 'primeng/checkbox';
@NgModule({
  exports: [ButtonModule, PanelModule, SliderModule, CheckboxModule]
})
export class PrimeModule { }
